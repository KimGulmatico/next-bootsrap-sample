import mongoose from 'mongoose'

export interface UserType {
    name: string
    age: Number
    bio: string
}

const UserSchema = new mongoose.Schema({
    name: { type: String},
    age: { type: Number },
    bio: { type: String }
})

export default mongoose.models.User || mongoose.model('User', UserSchema)