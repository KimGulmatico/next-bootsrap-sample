import type { NextPage } from 'next'
import { useRouter } from 'next/router'
import { useAtom } from "jotai";
import { counterAtom } from "../../store";

const Checker:NextPage = () => {
    const router = useRouter()
    const [{ counter }, setClock] = useAtom(counterAtom);
    return <div className="flex flex-col justify-center items-center h-screen space-y-5">
                <span className="text-6xl">{counter}</span>
                <span className="text-lg cursor-pointer hover:underline" onClick={() => router.back()}>Back</span>
           </div>
}

export default Checker