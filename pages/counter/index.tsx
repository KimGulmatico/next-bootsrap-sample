import type { NextPage } from 'next'
import Link from 'next/link'
import { useAtom } from "jotai";
import { counterAtom } from "../../store";

const Counter:NextPage = () => {
    const [{ counter }, setClock] = useAtom(counterAtom);
    return (<div className="flex flex-col justify-center items-center h-screen space-y-5">
                <span className="text-5xl">{counter}</span>
                <span onClick={() => setClock({counter: counter+1})} className="px-5 py-3 bg-blue-400 rounded-lg text-white text-4xl cursor-pointer">Increment</span>
                <Link href="/counter/checker"><a className="hover:underline">Check</a></Link>
            </div>)
}

export default Counter