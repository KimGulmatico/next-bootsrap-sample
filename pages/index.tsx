import type { NextPage } from 'next'
import Head from 'next/head'
import Image from 'next/image'
import classNames from 'classnames';
import styles from '../styles/Home.module.css'
import { atom, useAtom } from 'jotai'
import { useAtomValue, useHydrateAtoms } from 'jotai/utils'

const Home: NextPage = () => {
  return (
    <div className={classNames('text-white', styles.container)}>
      <div>
        <h1 className="text-bold text-9xl">Packages:</h1>
        <div className="flex flex-col space-y-3 text-2xl mt-10 text-gray-100">
          <p>Next.js - React framework with typescript</p>
          <p>Mongoose - Database framework</p>
          <p>Tailwind - CSS framework</p>
          <p>Jotai - State management</p>
        </div>
      </div>
    </div>
  )
}

export default Home
