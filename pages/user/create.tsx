import { useState } from 'react'
import { useRouter } from 'next/router'
import { mutate } from 'swr'
import { NextPage } from 'next'

const User:NextPage = () => {
  const router = useRouter()
  const contentType = 'application/json'
  const [message, setMessage] = useState('')


  const [form, setForm] = useState({
    name: '',
    age: 0,
    bio: '',
  })

  const postData = async (form:any) => {
    try {
      const res:any = await fetch('/api/user', {
        method: 'POST',
        headers: {
          Accept: contentType,
          'Content-Type': contentType,
        },
        body: JSON.stringify(form),
      })

      if (!res.ok) {
        throw new Error(res.status)
      }
    } catch (error) {
      setMessage('Failed to add user')
    }
  }

  const handleChange = (e:any) => {
    const target = e.target
    const value =
      target.name === 'poddy_trained' ? target.checked : target.value
    const name = target.name

    setForm({
      ...form,
      [name]: value,
    })
  }

  const handleSubmit = (e:any) => {
    e.preventDefault()
    postData(form)
  }

  return (
    <div className="h-screen w-screen flex justify-center items-center">
      <form onSubmit={handleSubmit} className="flex flex-col space-y-5">
        <label htmlFor="name">Name</label>
        <input
          className="border border-blue-500 rounded-md px-2 py-1"
          type="text"
          name="name"
          value={form.name}
          onChange={handleChange}
          required
        />

        <label htmlFor="owner_name">Age</label>
        <input
          className="border border-blue-500 rounded-md px-2 py-1"
          type="number"
          name="age"
          value={form.age}
          onChange={handleChange}
          required
        />

        <label htmlFor="species">Bio</label>
        <input
          className="border border-blue-500 rounded-md px-2 py-1"
          type="text"
          name="bio"
          value={form.bio}
          onChange={handleChange}
          required
        />

        <button type="submit" className="bg-blue-400 px-2 py-1 rounded text-white">
          Submit
        </button>
      </form>
      <p>{message}</p>
    </div>
  )
}

export default User