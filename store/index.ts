import { atom } from "jotai";

export const counterAtom = atom({
  counter: 0,
});